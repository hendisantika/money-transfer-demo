package com.hendisantika.transfers.money;

import com.hendisantika.transfers.money.controller.MainController;
import com.hendisantika.transfers.money.utils.MoneyTransferGatewayUtils;
import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.hsqldb.util.DatabaseManagerSwing;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:05
 */
public class StartMoneyTransferApp {
    public static HttpServer startServer() {
        URI mainUri = UriBuilder.fromUri(MoneyTransferGatewayUtils.HOST).port(MoneyTransferGatewayUtils.PORT).build();

        ResourceConfig resourceConfig = new ResourceConfig(MainController.class);

        HttpServer server = JdkHttpServerFactory.createHttpServer(mainUri, resourceConfig);
        return server;
    }

    public static void loadData(String showHsqlViewer) {
        MoneyTransferGatewayUtils.loadSampleData();

        if (showHsqlViewer.equalsIgnoreCase("yes"))
            DatabaseManagerSwing.main(
                    new String[]{"--url", "jdbc:hsqldb:mem:moneyTransferGateway", "--user", "sa", "--password", ""});
    }

    public static void main(String[] args) {

        loadData(args[0]);

        startServer();

    }
}
