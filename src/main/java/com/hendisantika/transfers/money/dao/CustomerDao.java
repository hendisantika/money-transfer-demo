package com.hendisantika.transfers.money.dao;

import com.hendisantika.transfers.money.model.Customer;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:17
 */
public interface CustomerDao {

    Customer findById(Integer id);

    void saveCustomer(Customer customer);

    List<Customer> getAllCustomers();

    void deleteCustomer(Customer customer);
}
