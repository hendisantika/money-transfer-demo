package com.hendisantika.transfers.money.dao;

import com.hendisantika.transfers.money.model.Account;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:19
 */
public class AccountDaoImpl extends AbstractDao<Integer, Account> implements AccountDao {

    public Account findById(Integer id) {
        return getByKey(id);
    }

    public void createAccount(Account account) {
        persist(account);

    }

    public List<Account> getAllAccounts() {
        return getAllEntities();
    }

    public void deleteAccount(Account account) {
        delete(account);

    }

    public void updateAccount(Account account) {
        merge(account);

    }
}
