package com.hendisantika.transfers.money.dao;

import com.hendisantika.transfers.money.model.Account;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:14
 */
public interface AccountDao {

    Account findById(Integer id);

    void createAccount(Account account);

    List<Account> getAllAccounts();

    void deleteAccount(Account account);

    void updateAccount(Account account);
}