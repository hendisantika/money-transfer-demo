package com.hendisantika.transfers.money.dao;

import com.hendisantika.transfers.money.model.Customer;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:20
 */
public class CustomerDaoImpl extends AbstractDao<Integer, Customer> implements CustomerDao {

    public Customer findById(Integer id) {

        return getByKey(id);
    }

    public void saveCustomer(Customer customer) {
        persist(customer);

    }

    public List<Customer> getAllCustomers() {
        return getAllEntities();
    }

    public void deleteCustomer(Customer customer) {
        delete(customer);

    }

}
