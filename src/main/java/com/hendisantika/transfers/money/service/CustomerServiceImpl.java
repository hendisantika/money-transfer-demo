package com.hendisantika.transfers.money.service;

import com.hendisantika.transfers.money.dao.CustomerDao;
import com.hendisantika.transfers.money.dao.CustomerDaoImpl;
import com.hendisantika.transfers.money.model.Customer;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:20
 */
public class CustomerServiceImpl implements CustomerService {
    CustomerDao customerDao = new CustomerDaoImpl();

    public Customer findById(Integer id) {
        return customerDao.findById(id);
    }

    public void saveCustomer(Customer customer) {
        customerDao.saveCustomer(customer);

    }

    public List<Customer> getAllCustomers() {
        return customerDao.getAllCustomers();
    }

    public void deleteCustomer(Customer customer) {
        customerDao.deleteCustomer(customer);

    }
}
