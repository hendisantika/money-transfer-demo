package com.hendisantika.transfers.money.service;

import com.hendisantika.transfers.money.dao.AccountDao;
import com.hendisantika.transfers.money.dao.AccountDaoImpl;
import com.hendisantika.transfers.money.exception.MoneyTransferGatewayException;
import com.hendisantika.transfers.money.model.Account;
import com.hendisantika.transfers.money.utils.MoneyTransferGatewayUtils;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:12
 */
public class AccountServiceImpl implements AccountService {
    AccountDao accountDao = new AccountDaoImpl();

    public Account findById(Integer id) {
        return accountDao.findById(id);
    }

    public void createAccount(Account account) {
        accountDao.createAccount(account);

    }

    public List<Account> getAllAccounts() {
        return accountDao.getAllAccounts();
    }

    public void withdraw(Account account, Double amount) throws MoneyTransferGatewayException {
        if (account.getBalance() >= amount) {
            account.setBalance(account.getBalance() - amount);
            accountDao.updateAccount(account);
        } else
            throw new MoneyTransferGatewayException("The account does not have sufficient balance");

    }

    public void deposit(Account account, Double amount) throws MoneyTransferGatewayException {

        account.setBalance(account.getBalance() + amount);
        accountDao.updateAccount(account);
    }

    public void transferTo(Integer fromAccountId, Integer toAccountId, Double balance)
            throws MoneyTransferGatewayException {

        Account from = accountDao.findById(fromAccountId);
        Account to = accountDao.findById(toAccountId);

        Double transactionFee = MoneyTransferGatewayUtils.TRANFER_TRANSACTION_FEE;

        if (from == null || to == null)
            throw new MoneyTransferGatewayException("One of the accounts does not exist");

        if (!from.getType().equals(to.getType()))
            throw new MoneyTransferGatewayException(
                    "The account types must be the same in order to have a successful transfer between accounts");

        if (from.getBalance() >= (balance + transactionFee)) {
            withdraw(from, (balance + transactionFee));
            deposit(to, balance);
        } else
            throw new MoneyTransferGatewayException("The account does not have sufficient balance to transfer money");


    }

    public void deleteAccount(Account account) {
        accountDao.deleteAccount(account);

    }
}
