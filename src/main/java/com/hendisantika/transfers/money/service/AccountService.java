package com.hendisantika.transfers.money.service;

import com.hendisantika.transfers.money.exception.MoneyTransferGatewayException;
import com.hendisantika.transfers.money.model.Account;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:10
 */
public interface AccountService {

    Account findById(Integer id);

    void createAccount(Account account);

    List<Account> getAllAccounts();

    void withdraw(Account account, Double amount) throws MoneyTransferGatewayException;

    void deposit(Account account, Double amount) throws MoneyTransferGatewayException;

    void deleteAccount(Account account);

    void transferTo(Integer fromAccountId, Integer toAccountId, Double balance) throws MoneyTransferGatewayException;

}