package com.hendisantika.transfers.money.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:11
 */
public class MoneyTransferGatewayException extends Exception {

    private static final long serialVersionUID = 1L;

    public MoneyTransferGatewayException(String message) {
        super(message);
    }

    public MoneyTransferGatewayException(String message, Throwable cause) {
        super(message, cause);
    }
}