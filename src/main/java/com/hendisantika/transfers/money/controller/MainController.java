package com.hendisantika.transfers.money.controller;

import com.hendisantika.transfers.money.exception.MoneyTransferGatewayException;
import com.hendisantika.transfers.money.model.Account;
import com.hendisantika.transfers.money.model.Customer;
import com.hendisantika.transfers.money.service.AccountService;
import com.hendisantika.transfers.money.service.AccountServiceImpl;
import com.hendisantika.transfers.money.service.CustomerService;
import com.hendisantika.transfers.money.service.CustomerServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 08:22
 */
@Path("/transferGateway")
public class MainController {
    AccountService accountService = new AccountServiceImpl();
    CustomerService customerService = new CustomerServiceImpl();

    @GET
    @Path("/getAllCustomers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @GET
    @Path("/getAllAccounts")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @GET
    @Path("/getAccountById/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Account getAccountById(@PathParam("id") Integer id) {
        return accountService.findById(id);
    }

    @GET
    @Path("/getCustomerById/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Customer getCustomerById(@PathParam("id") Integer id) {
        return customerService.findById(id);
    }

    @POST
    @Path("/createAccount")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(Account account) {
        this.accountService.createAccount(account);
        return Response.status(200).type("text/plain").entity("success").build();
    }

    @POST
    @Path("/createCustomer")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(Customer customer) {
        this.customerService.saveCustomer(customer);
        return Response.status(200).type("text/plain").entity("success").build();
    }


    @PUT
    @Path("/deposit/{id}/{balance}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deposit(@PathParam("id") Integer id, @PathParam("balance") Double balance) throws MoneyTransferGatewayException {
        accountService.deposit(accountService.findById(id), balance);
        return Response.status(200).type("text/plain").entity("success").build();
    }

    @PUT
    @Path("/withdraw/{id}/{balance}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response withdraw(@PathParam("id") Integer id, @PathParam("balance") Double balance) throws MoneyTransferGatewayException {
        accountService.withdraw(accountService.findById(id), balance);
        return Response.status(200).type("text/plain").entity("success").build();
    }

    @PUT
    @Path("/transferMoney/{fromId}/{toId}/{balance}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transferMoney(@PathParam("fromId") Integer fromId, @PathParam("toId") Integer toId, @PathParam("balance") Double balance) throws MoneyTransferGatewayException {
        accountService.transferTo(fromId, toId, balance);
        return Response.status(200).type("text/plain").entity("success").build();
    }

    @DELETE
    @Path("/deleteCustomer")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteCustomer(Customer customer) {
        this.customerService.deleteCustomer(customerService.findById(customer.getId()));
        return Response.status(200).type("text/plain").entity("success").build();
    }

    @DELETE
    @Path("/deleteAccount")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAccount(Account account) {
        this.accountService.deleteAccount(accountService.findById(account.getId()));
        return Response.status(200).type("text/plain").entity("success").build();
    }




}
