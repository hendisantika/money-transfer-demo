package com.hendisantika.transfers.money;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hendisantika.transfers.money.model.Account;
import com.hendisantika.transfers.money.model.Customer;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by IntelliJ IDEA.
 * Project : money-transfer-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-15
 * Time: 12:31
 */
public class MoneyTransferApiTests {
    private static String URI_GETALLCUSTOMERS = "http://localhost:7777/transferGateway/getAllCustomers";
    private static String URI_GETALLACCOUNTS = "http://localhost:7777/transferGateway/getAllAccounts";
    private static String URI_POST_CREATECUSTOMER = "http://localhost:7777/transferGateway/createCustomer";
    private static String URI_POST_CREATEACCOUNT = "http://localhost:7777/transferGateway/createAccount";

    @BeforeClass
    public static void runFirstBeforeAlltests() {
        StartMoneyTransferApp.loadData("no");//If you want to see the hsqldb viewer ,set this argument to "yes"
        StartMoneyTransferApp.startServer();
    }

    public static <T> T retrieveResourceFromResponse(HttpResponse response, Class<T> clazz) throws IOException {
        String jsonFromResponse = EntityUtils.toString(response.getEntity());
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(jsonFromResponse, clazz);
    }

    public String getCreateCustomerPayload() {
        String payload = "{" + "\"name\": \"TOM\", " + "\"surname\": \"JACKSON\", " + "\"accounts\":[]"
                + "}";
        return payload;
    }

    public String getCreateAccountPayload() {
        String payload = "{" + "\"customer\": {\"id\":1},\"type\":\"CHECKING\",\"balance\":\"4000\""
                + "}";
        return payload;
    }

    @Test
    public void testGetCustomersStatusCode() throws IOException {

        HttpUriRequest request = new HttpGet(URI_GETALLCUSTOMERS);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, httpResponse.getStatusLine().getStatusCode());
    }

    @Test
    public void testGetCustomersReturnMediaType() throws IOException {
        String mimeType = "application/json";
        HttpUriRequest request = new HttpGet(URI_GETALLCUSTOMERS);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        String returnMimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();

        assertEquals(mimeType, returnMimeType);
    }

    @Test
    public void testGetCustomersResponseSize() throws IOException {
        HttpUriRequest request = new HttpGet(URI_GETALLCUSTOMERS);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        List<Customer> customers = retrieveResourceFromResponse(httpResponse, List.class);

        assertThat(customers, hasSize(1));

    }

    @Test
    public void testGetAccountsStatusCode() throws IOException {

        HttpUriRequest request = new HttpGet(URI_GETALLACCOUNTS);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, httpResponse.getStatusLine().getStatusCode());
    }

    @Test
    public void testGetAccountsReturnMediaType() throws IOException {
        String mimeType = "application/json";
        HttpUriRequest request = new HttpGet(URI_GETALLACCOUNTS);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
        String returnMimeType = ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();

        assertEquals(mimeType, returnMimeType);
    }

    @Test
    public void testGetAccountsResponseSize() throws IOException {
        HttpUriRequest request = new HttpGet(URI_GETALLACCOUNTS);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        List<Account> accounts = retrieveResourceFromResponse(httpResponse, List.class);

        assertThat(accounts, hasSize(3));

    }

    @Test
    public void testCreateCustomer() throws IOException {

        StringEntity entity = new StringEntity(getCreateCustomerPayload(), ContentType.APPLICATION_JSON);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost postRequest = new HttpPost(URI_POST_CREATECUSTOMER);
        postRequest.setEntity(entity);

        HttpResponse response = httpClient.execute(postRequest);

        assertEquals(200, response.getStatusLine().getStatusCode());

    }

    @Test
    public void testCreateAccount() throws IOException {

        StringEntity entity = new StringEntity(getCreateAccountPayload(), ContentType.APPLICATION_JSON);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost postRequest = new HttpPost(URI_POST_CREATEACCOUNT);
        postRequest.setEntity(entity);

        HttpResponse response = httpClient.execute(postRequest);

        assertEquals(200, response.getStatusLine().getStatusCode());

    }

}
